<?php

/**
 * @file
 * Test case for vocabulary_queue module
 */

/**
 * Tests the relevant functionality provided by the vocabulary_queue module
 */
class VocabularyQueueWebTestCase extends DrupalWebTestCase {
  /**
   * Info for simpletest.module.
   */
  public static function getInfo() {
    return array(
      'name' => 'Vocabulary Queue',
      'description' => 'Test Vocabulary Queue functionality.',
      'group' => 'Nodequeue',
    );
  }

  protected $vocabularyQueueVocabulary;
  protected $vocabularyQueueNodetype;
  protected $vocabularyQueuePreExistingTerm;

  /**
   * Setup for individual tests.
   */
  public function setUp() {
    // Enable any modules required for the test.
    $this->profile = 'testing';
    parent::setUp(array('vocabulary_queue'));

    // Setup vocabulary for testing.
    $vocabulary = (object) array(
      'name' => 'vocabulary_queue_vocabulary',
      'machine_name' => 'vocabulary_queue_vocabulary',
      'description' => 'Description of vocabulary_queue.',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    );
    taxonomy_vocabulary_save($vocabulary);
    $this->vocabularyQueueVocabulary = taxonomy_vocabulary_machine_name_load('vocabulary_queue_vocabulary');

    // Setup taxonomy term for testing.
    $term = (object) array(
      'vid' => $this->vocabularyQueueVocabulary->vid,
      'name' => 'vocabulary_queue_term',
    );

    taxonomy_term_save($term);

    $tid = db_select('taxonomy_term_data', 'ttd')
      ->fields('ttd', array('tid'))
      ->condition('name', 'vocabulary_queue_term')
      ->condition('vid', $this->vocabularyQueueVocabulary->vid)
      ->execute()
      ->fetchAssoc();
    $this->vocabularyQueuePreExistingTerm = taxonomy_term_load($tid['tid']);

    // Setup a node type for testing.
    $this->vocabularyQueueNodetype = $this->drupalCreateContentType();

    // Setup user for testing vocabulary_queue.
    $permissions = array(
      'administer nodequeue',
      'manipulate queues',
      'manipulate all queues',
      'administer taxonomy',
    );

    $this->admin_user = $this->drupalCreateUser($permissions);
    $this->drupalLogin($this->admin_user);

    // cache_clear_all();
  }

  /**
   * Assert that free tag fields are present on site term.
   */
  public function testVocabularyQueue() {
    $this->assertTrue(module_exists('taxonomy'), 'Dependency "taxonomy" enabled.');
    $this->assertTrue(module_exists('nodequeue'), 'Dependency "nodequeue" enabled.');

    // Create a vocabulary queue.
    $edit = array(
      'title' => 'vocabulary_queue_vocabulary_queue',
      'name' => 'vocabulary_queue',
      'subqueue_title' => 'Subqueue for %subqueue',
      'vocabularies[' . $this->vocabularyQueueVocabulary->name . ']' => TRUE,
      'types[' . $this->vocabularyQueueNodetype->name . ']' => TRUE,
    );
    $this->drupalPost('admin/structure/nodequeue/add/vocabulary_queue', $edit, t('Submit'));

    // Assert that the vocabulary queue got created.
    $nqq = db_select('nodequeue_queue', 'nqq')
      ->fields('nqq', array('qid'))
      ->condition('owner', 'vocabulary_queue')
      ->execute()
      ->fetchAssoc();

    $vq = db_select('vocabulary_queue', 'vq')
      ->fields('vq')
      ->execute()
      ->fetchAssoc();

    $this->assertTrue(is_array($nqq) AND is_array($vq), 'Vocabulary Queue created.');

    // Assert that a subqueue exists for the taxonomy term created in setUp().
    $nqsq = db_select('nodequeue_subqueue', 'nqsq')
      ->fields('nqsq', array('sqid'))
      ->condition('reference', $this->vocabularyQueuePreExistingTerm->tid)
      ->execute()
      ->fetchAssoc();

    $this->assertTrue(is_array($nqsq), 'Vocabulary Subqueue created for pre-existing term.');

    // Add another taxonomy term to test implementation of
    // hook_taxonomy_term_insert().
    $term = (object) array(
      'vid' => $this->vocabularyQueueVocabulary->vid,
      'name' => 'vocabulary_queue_another_term',
    );
    taxonomy_term_save($term);

    $tid = db_select('taxonomy_term_data', 'ttd')
      ->fields('ttd', array('tid'))
      ->condition('name', 'vocabulary_queue_another_term')
      ->condition('vid', $this->vocabularyQueueVocabulary->vid)
      ->execute()
      ->fetchAssoc();
    $another_term = taxonomy_term_load($tid['tid']);

    // Assert that a subqueue exists for the taxonomy term created above.
    $nqsq = db_select('nodequeue_subqueue', 'nqsq')
      ->fields('nqsq', array('sqid'))
      ->condition('reference', $another_term->tid)
      ->execute()
      ->fetchAssoc();

    $this->assertTrue(is_array($nqsq), 'Vocabulary Subqueue created on hook_taxonomy_term_insert().');

    // Assert that subqueue got renamed on implementation of
    // hook_taxonomy_term_update().
    $another_term->name = 'vocabulary_queue_another_term_new_name';
    taxonomy_term_save($another_term);

    $nqsq = db_select('nodequeue_subqueue', 'nqsq')
      ->fields('nqsq', array('title'))
      ->condition('reference', $another_term->tid)
      ->execute()
      ->fetchAssoc();

    $this->assertEqual($nqsq['title'], 'vocabulary_queue_another_term_new_name', 'Vocabulary Subqueue title updated on hook_taxonomy_term_update().');

    // Delete a term with a related subqueue to test implementation of
    // hook_taxonomy_term_delete().
    taxonomy_term_delete($another_term->tid);

    // Assert that a subqueue exists for the taxonomy term created above.
    $nqsq = db_select('nodequeue_subqueue', 'nqsq')
      ->fields('nqsq', array('sqid'))
      ->condition('reference', $another_term->tid)
      ->execute()
      ->fetchAssoc();

    $this->assertFalse(is_array($nqsq), 'Vocabulary Subqueue deleted on hook_taxonomy_term_delete().');

    // Assert that a subqueue still exists for the taxonomy term created in
    // setUp().
    $nqsq = db_select('nodequeue_subqueue', 'nqsq')
      ->fields('nqsq', array('sqid'))
      ->condition('reference', $this->vocabularyQueuePreExistingTerm->tid)
      ->execute()
      ->fetchAssoc();

    $this->assertTrue(is_array($nqsq), 'Did not delete more Vocabulary Subqueues than expected on hook_taxonomy_term_delete().');

    // Assert queue and subqueue got listed in vocabulary edit form and taxonomy
    // term edit form.
    $this->drupalGet('admin/structure/taxonomy/' . $this->vocabularyQueueVocabulary->machine_name . '/edit');

    $this->assertLinkByHref('admin/structure/taxonomy/' . $this->vocabularyQueueVocabulary->machine_name . '/vocabulary_queue', 0, 'Vocabulary Queue listed in vocabulary edit form.');

    $this->drupalGet('taxonomy/term/' . $this->vocabularyQueuePreExistingTerm->tid . '/edit');

    $this->assertLinkByHref('taxonomy/term/' . $this->vocabularyQueuePreExistingTerm->tid . '/vocabulary_queue', 0, 'Vocabulary Queue listed in taxonomy term edit form.');
  }
}
